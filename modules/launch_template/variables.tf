variable "name" {
  description = "Name of the launch template"
  type        = string
  default     = "my-launch-template"
}

variable "block_device_mappings" {
  description = "Block device mappings for attached volumes"
  type        = list(object({
    device_name = string
    ebs {
      volume_size = number
    }
  }))
  default = [
    {
      device_name = "/dev/sdf"
      ebs = {
        volume_size = 20
      }
    }
  ]
}

variable "capacity_reservation_specification" {
  description = "Capacity reservation preferences"
  type        = object({
    capacity_reservation_preference = string
  })
  default = {
    capacity_reservation_preference = "open"
  }
}

variable "cpu_options" {
  description = "CPU-related options"
  type        = object({
    core_count        = number
    threads_per_core = number
  })
  default = {
    core_count        = 4
    threads_per_core = 2
  }
}

variable "credit_specification" {
  description = "CPU credit customization"
  type        = object({
    cpu_credits = string
  })
  default = {
    cpu_credits = "standard"
  }
}

# ... Add more variables for other parameters (e.g., instance_type, key_name, monitoring, etc.)

# Example variable for instance type
variable "instance_type" {
  description = "EC2 instance type"
  type        = string
  default     = "t2.micro"
}

# Example variable for key name
variable "key_name" {
  description = "SSH key pair name"
  type        = string
  default     = "my-key-pair"
}
