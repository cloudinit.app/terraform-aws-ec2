resource "aws_launch_template" "this" {
  name = "my-launch-template"

  block_device_mappings {
    device_name = "/dev/sdf"
    ebs {
      volume_size = 20
    }
  }

  capacity_reservation_specification {
    capacity_reservation_preference = "open"
  }

  cpu_options {
    core_count        = 4
    threads_per_core = 2
  }

  credit_specification {
    cpu_credits = "standard"
  }

  disable_api_stop = true

  disable_api_termination = true

  elastic_gpu_specifications {
    type = "eg1.medium"
  }

  enclave_options {
    enabled = true
  }

  hibernation_options {
    configured = true
  }

  iam_instance_profile {
    name = "my-instance-profile"
  }

  instance_market_options {
    market_type = "spot"
  }

  instance_type = "t2.micro"

  kernel_id = "aki-12345678"

  key_name = "my-key-pair"

  license_specifications {
    license_configuration_arn = "arn:aws:license-manager:us-east-1:123456789012:license-configuration:my-license"
  }

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "optional"
    http_put_response_hop_limit = 2
  }

  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = true
    device_index                = 0
  }

  placement {
    availability_zone = "us-east-1a"
  }

  ram_disk_id = "ari-12345678"

  vpc_security_group_ids = ["sg-12345678"]

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name        = "MyLaunchTemplate"
      Environment = "Production"
    }
  }

  user_data = <<-EOT
    #!/bin/bash
    echo "Hello from user data!"
  EOT
}
