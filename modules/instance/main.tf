resource "aws_instance" "this" {
  ami           = var.ami
  instance_type = var.instance_type
  key_name      = var.key_name
  monitoring    = var.monitoring
  subnet_id     = var.subnet_id

  vpc_security_group_ids = var.vpc_security_group_ids

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}
